echo -n "Nombre del contenedor:"
read nombre
#creamos un conenedor con el nombre introducido sin configuracion de red
docker run -dit --name $nombre --network none --privileged ov-image bash

echo "Introduzca el numero del nombre del puerto ethernet virtual del contenedor (veth\$numero):"
read n

veth0=veth$n
veth1=veth$((n+1))

#creamos enlaces entre las veth
ip link add $veth0 type veth peer name $veth1
#Conectamos el bridge
ip link set $veth0 master bridge_d
#Pasamos a up el veth0
ip link set dev $veth0 up

ip link set $veth2 netns $(docker inspect --format '{{.State.Pid}}' $nombre)

echo -n 'Introduce el ultimo numero de la direccion ip 192.168.111.i: '
read i

ip=192.168.111.$i

docker exec -d $nombre ip addr add $ip/24 dev $veth1
docker exec -d $nombre ip link set dev $veth1 up
docker exec -d $nombre ip route add 0.0.0.0/0 via 192.168.111.1

echo -n "Asignar puerto al contenedor: "
read p
echo -n "Enter host IP:"
read h

echo "añadiendo regla de iptable"
iptables -t nat -A PREROUTING -p tcp --dport $p -d $h -j DNAT -- to-destination $ip

echo "Contenedor $nombre creado con éxito"