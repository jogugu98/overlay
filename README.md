# Overlay
### Alcance del proyecto
Se ha llegado al escenario 2 del trabajo. 
Los contenedores pueden comunicar con cualquier nodo alcanzable desde el host en
su red de área local, es decir, desde nodos que puedan alcanzar al host se pueda establecer comunicación
con un contenedor.

### Configuracion inicial
En este caso se han creado dos maquinas virtuales ubuntu 20.04, actuando como nodos.
En un nodo se debe ejecutar el script contman, que construye dos contenedores y un bridge.
Es importante que en las maquinas virtuales se configure una red Nat.

### Ejecución

En un nodo ejecutar el script contman.sh.
Este script lo que hace es crear dos contenedores ademas del bridge.

Para comprobar el correcto funcionamiento de toda la configuración, se puede ejecutar siguiente comando desde un nodo que no sea el host:
```sh
$ nc host_ip container_port
```
