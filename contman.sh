#!/bin/bash
docker build -t ov-image .
eth=`ls -l /sys/class/net/ | grep -v virtual | cut -d' ' -f9`

ip link add bridge_d type bridge
ip addr add 192.168.111.1/24 brd + dev bridge_d

cont=1

while [ $cont -lt 3 ]
do
    ./crear_contenedores.sh
    echo "Contenedor $cont creado."
    cont=$(( $cont + 1 ))
done


ip link set bridge_d up
echo "Se ha creado el bridge"

bash -c "echo 0 > /proc/sys/net/bridge/bridge-nf-call-iptables"
iptables -t nat -A POSTROUTING -s 10.1.1.0/24 -o $eth -j MASQUERADE
echo "Done!"



